import React, {useEffect, useState} from 'react';
import restCountries from './services/restCountries';


import CountryCard from './components/CountryCard'
import CardContainer from './components/CardContainer'
import Weather from './components/Weather'
import CountrySearch from './components/CountrySearch'

function App() {

const [countriesList, setCountriesList] = useState([])
const [countrySearch, setCountrySearch] = useState('')
  const handleSetSearch = (event) => {
    setCountrySearch(event.target.value)
  }

  const filteredCountries = countriesList.filter(country =>
    country.name.toLowerCase().search(countrySearch.toLowerCase()) !==
    -1,
  )

  useEffect(() => {
    restCountries().then(countries => {
      setCountriesList(countries)
    })
    }, [])

  return (
<CardContainer>

  <CountrySearch       value={countrySearch}
                       onChange={handleSetSearch}
                       />


{filteredCountries.length < 7 ?
filteredCountries.map(country => 

<CountryCard  key={country.name}
              capital={country.capital}
              flag={country.flag}
              name={country.name}
              language={country.languages[0].name}
              population={country.population}
                                               >
              <Weather capital={country.capital}/>
</CountryCard>
)
:
<h1>More specific search required</h1>}

</CardContainer>

  );
}

export default App;
