import axios from 'axios'
const baseUrl = "https://api.openweathermap.org/data/2.5/weather?q=";
const appid = "fc5900ef4a8f46fde3f8531538170955"

const restWeather = async capital => {
  const response = await axios

  .get(`${baseUrl}${capital}&appid=${appid}&units=imperial`);

  return response.data;
  console.log(response.data)
};

export default restWeather
