import React, { Component } from 'react';
import styled, { keyframes } from 'styled-components';
import logo from '../logo.svg';


const swingIn = keyframes`
    0% {
      -webkit-transform: translateY(-1000px) scaleY(2.5) scaleX(0.2);
              transform: translateY(-1000px) scaleY(2.5) scaleX(0.2);
      -webkit-transform-origin: 50% 0%;
              transform-origin: 50% 0%;
      -webkit-filter: blur(40px);
              filter: blur(40px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0) scaleY(1) scaleX(1);
              transform: translateY(0) scaleY(1) scaleX(1);
      -webkit-transform-origin: 50% 50%;
              transform-origin: 50% 50%;
      -webkit-filter: blur(0);
              filter: blur(0);
      opacity: 1;
    }

`
const Card = styled.div`
animation: ${swingIn} 0.6s cubic-bezier(0.230, 1.000, 0.320, 1.000) both;
width: 450px;
height: 250px;
background-color: #fff;
background: #fff;
box-shadow: 0 8px 16px -8px rgba(0,0,0,0.4);
border-radius: 6px;
overflow: hidden;
position: relative;
margin: 1.5rem;
:h1 {
    text-align: center;
}
`

const CardFront = styled.div`
width: 300px;
height: 100%;
position: absolute;
top: 0;
right: 0;
z-index: 1;
box-sizing: border-box;
padding: 1rem;
padding-top: 0;
`;

const CardFlag = styled.img`
display: flex;
flex: 1;
padding: 10px;
margin: 10px;
justify-content: center;
height: 75px;
width: 100px;
`

const Additional = styled.div`
position: absolute;
width: 150px;
height: 100%;
background: #888888;
transition: width 0.4s;
overflow: hidden;
z-index: 2;
:hover {
    width: 100%;
    border-radius: 0 5px 5px 0;

`;

const UserCard = styled.div`
width: 150px;
height: 100%;
display:flex;
position: relative;
float: left;
::after {
  content: "";
  display: block;
  position: absolute;
  top: 10%;
  right: -2px;
  height: 80%;
  border-left: 2px solid rgba(0,0,0,0.025);
}
`;

const Population = styled.div`
position: absolute;
left: 50%;
-webkit-transform: translate(-50%, -50%);
top: 15%;
color: #fff;
text-transform: uppercase;
font-size: 0.75em;
font-weight: bold;
background: rgba(0,0,0,0.15);
padding: 0.125rem 0.75rem;
border-radius: 100px;
white-space: nowrap;
`;

const Languages = styled(Population)`
top: 25%;
`

const Capital = styled(Population)`
top: 35%;
`

const Weather = styled(Population)`
top: 55%;
`

const AdditionalInfo = styled.div`
width: 300px;
float: left;
position: absolute;
left: 150px;
height: 100%;
span {
    float: right;
}
`;
const Stats = styled.div`
font-size: .5rem;
text-align: center;
justify-content: center;
display: flex;
position: absolute;
bottom: 5rem;
left: 2rem;
right: 2rem;
color: #fff;
`
const CountryName = styled.h3`
text-align: center;
padding: .5em;
top: 50%;
position: absolute;
`



function CountryCard(props) {
    return(
        <Card>
            <Additional>
                <UserCard>
                    <CardFlag src={props.flag}></CardFlag>
                    <CountryName>{props.name}</CountryName>
                    <AdditionalInfo>
                        <Population>Population: {props.population}</Population>
                        <Languages>Language: {props.language}</Languages>
                        <Capital>Capital: {props.capital}</Capital>
                        <Stats>
                            <Weather>Weather in {props.capital}</Weather>
                        </Stats>
                    </AdditionalInfo>
                </UserCard>
            </Additional>
            <CardFront>  
                {props.children}
            </CardFront>
        </Card>
    )
}
export default CountryCard


