import React, {useContext} from 'react'
import '../App.css';


function CountrySearch(props) {
return (

  <div className="page slide-in-blurred-top">
  <label className="field a-field a-field_a1">
    <input onChange={props.onChange} value={props.value} className="field__input a-field__input" placeholder="e.g. 'SOUTH AFRICA'" required/>
    <span className="a-field__label-wrap">
      <span className="a-field__label">Type a country...</span>
    </span>
  </label>
  <div className='page'>

<input className='switch' type="checkbox" onClick={props.onClick} id="switch" /><label className='switch' for="switch">Toggle</label>


  </div>
  </div>

  )
}

export default CountrySearch