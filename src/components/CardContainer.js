import React, { Component } from 'react';
import styled from 'styled-components';

const Container = styled.div`
justify-content: center;
display: flex;
flex-direction: row;
flex-wrap: wrap;
`;

function CardContainer(props) {
    return (
        <Container>
            {props.children}
        </Container>
    )
}

export default CardContainer