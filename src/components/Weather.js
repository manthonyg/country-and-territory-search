import React, { useState, useEffect } from "react";
import restWeather from "../services/restWeather";
import styled from 'styled-components'

const Weather = ({ capital  }) => {
const baseURL = 'http://openweathermap.org/img/wn/';
const iconSize = '@2x.png'


const [weather, setWeather] = useState();

  useEffect(() => {
  restWeather(capital)
  .then(res => setWeather(res))
  }, []
  )

  const CardFront = styled.div`
  width: 300px;
  height: 100%;
  position: absolute;
  top: 0;
  right: 0;
  z-index: 1;
  box-sizing: border-box;
  padding: 1rem;
 
  `;

return (

  !weather ?
  <CardFront>
     <h2>No weather data currently available</h2>
   </CardFront>
   :
   <CardFront>
           <h2>Weather</h2>
            <img className='weather-icon' src={'http://openweathermap.org/img/wn/' + weather.weather.map(i => i.icon) + '@2x.png'}/>
           <h4>City of {capital}</h4>
             <h3>{weather.weather.map(w => w.main)} - {weather.weather.map(w => w.description)}</h3>
             <br/>
             <br/>
             <span>{weather.wind.speed}mp/h</span>
             <br/>
             <span>Precip 0%</span>
             <br/>
             <span>High: {weather.main.temp_max} <span class="dot">•</span> Low: {weather.main.temp_min}</span>
    </CardFront>

)

}
export default Weather;